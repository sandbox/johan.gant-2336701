<?php

/**
 * Admin form for issuing purge requests.
 */
function purge_expirepath_admin_form() {
  $destination = drupal_get_destination();
  if (!array_key_exists('destination', $destination)) {
    $destination = '';
  }
  else {
    $destination = $destination['destination'];
  }

  $form['purge_expirepath_path_to_expire'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to expire'),
    '#description' => t('Enter the internal path you wish to purge.</br>ie: <em>content/my-page</em></br>and <strong>NOT</strong>: <em>http://www.example.com/content/my-page</em>'),
    '#required' => TRUE,
    '#size' => 120,
  );
  $form['purge_expirepath_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
  );

  return $form;
}

/**
 * Submit handler for purge_expirepath_admin_form.
 */
function purge_expirepath_admin_form_submit(&$form, &$form_state) {
  // Don't want possible nasty XSS things being sent to external proxy,
  // however unlikely it is to do anything too awful.
  $url = filter_xss($form_state['values']['purge_expirepath_path_to_expire']);

  // Trigger hook_expire_cache to purge this specific URL.
  module_invoke_all('expire_cache', array($url));
}
